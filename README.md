 # PROGRAMACION LOGICA Y FUNCIONAL -SCC1019-ISA-20213
 ![](imagenes/tec.png)
 # MATEMATICAS
 ## Conjuntos, Aplicaciones y funciones (2002)
 ## Descripcion 
 trata sobre que son los conjuntos que es una aplicacion y una funcion 

 ```plantuml
@startmindmap
<style>
mindmapDiagram{
   
    BackGroundColor  	#FAEBD7
    LineBorder none
    LineColor black
    node{
        LineColor none
        
        BackGroundColor  #00FFFF
    }
}
</style>
title  Conjuntos, Aplicaciones y funciones (2002).
*[#red] conjuntos 
**_ son 
***[#green] pensamientos abstractos \n sobre la matematica 
**_ se denomina como
*** una coleccion o reunion de elementos 
****_ tiene una 
***** relacion de pertenencia 
******_ donde 
******* se puede saber si un elemento esta\n  o no esta dentro del conjunto 
**_ tiene 
***[#green] conceptos 
****_ como 
***** inclusión de conjuntos 
******_ es 
******* la relacion que contiene un conjunto \n dentro de otroconjunto 
********_ si 
********* todo elemento de conjunto 1 esta \n en el segundo conjunto pertenece en conjunto 1 al 2
*****[#green] cardinalidad de un conjunto 
******_ es
******* el numero de elementos que conforman \n el conjunto 
******_ propiedades
*******  la cardinalida de una union 
********_ es 
********* la cardinalidad del conjunto 1 + cardinalidad del conjunto 2 - la interseccion de estos 
******* acotacion de cardinales 
********_ es una relacion 
********* para ver que cardinal es mayor o menor \n o cardinalidad minima o maxima 

*****[#green] operaciones 
******_ como 
******* interseccion 
********_ que se define 
********* los conjuntos que pertenecen a ambos conjuntos 
******* Union 
********_ que se define 
********* algunos de los elementos pertenecen a ambos conjuntos
******* complementacion 
********_ que se define 
*********  son los elementos    que no pertenecen al conjunto\n estudiado
**_ tiene 
***[#green] tipos de conjuntos 
****_ el 
***** universal
******_ es un 
******* conjunto de referencia donde se aplica la teoria 
***** vacio 
******_ es un 
*******[#green] conjunto de necesidad logica donde \n no tiene elementos

**_ se
***[#green] representas por diagramas de venn 
****_ o 
***** por circulos u ovalos 
******_ inventado por 
******* logico llamado John Venn.
********_ en el año
********* 1880
**_ tiene
***[#green] aplicacion 
****_ se 
***** denomina a cualquier transformacion\n o cambio como aplicacion 
******_ se identifica
******* el elemento que cambia 
********_ ejemplo 
********* una esponja que se somete al agua cambia 
***[#green] imagen 
****_ es 
***** En general, el conjunto imagen es un subconjunto del codominio, y cuando el\n rango coincide con el codominio se dice que la función es sobreyectiva o suprayectiva.
*** imagen inversa 
****_ es
*****  contraimagen de una aplicación es la aplicación que a cada subconjunto del \n conjunto final de la aplicación le hace corresponder el conjunto de elementos del conjunto inicial cuya imagen se encuentra en este conjunto

***[#green] funciones
****_ que son 
*****  una magnitud es función de otra si el valor de la primera depende del valor de la segunda

@endmindmap
```
[Conjuntos, Aplicaciones y funciones (2002)](https://canal.uned.es/video/5a6f1b77b1111f15098b4609)

# Funciones (2010)
## Descripcion 
 el mapa trata sobre la definicion de una funcion.
 ```plantuml
@startmindmap
<style>
mindmapDiagram{
   
    BackGroundColor  	#FAEBD7
    LineBorder none
    LineColor black
    :depth(1) {
        BackGroundColor  white
    }
    node{
        LineColor none
        
        BackGroundColor  #00FFFF
    }
}
</style>
title  funciones.
*[#green] funciones 
**_ son 
***[#orange] Las funciones\n son reglas que \n relacionan los elementos de\n un conjunto con los elementos de un segundo conjunto\n Cuando una magnitud depende de otra\n, se dice que está en función de ésta
****_ como
***** el cambio de temperatura\n la rapidez de un automovil  
**_ tiene 
***[#orange] caracteristicas 
****_ como 
***** maximos
******_ son 
******* una función f son los valores más grandes (máximos) 
***** minimos 
******_ son 
******* valores más pequeños (mínimos)
**_ tiene 
***[#orange] limite 
****_ es 
*****  son susimagenes que estan cerca de un determinado valor 
******_ y son 
******* continuas 
********_ y en 
********* una descontinuidad 
**********_ la funcion 
*********** no tiene limite 
**_ tiene 
***[#orange] la derivada 
****_ contiene 
***** funciones sencillas 
***** cambios en la tangente 
***** representaciones de cambios fisicos
***** aproximacion por la derecha e izquierda
****_ que es 
***** la proximacion a un valor , \n con una expresion mas simple 
******_ ejemplo 
******* la velocidad que recorremos en un sentimental 
**_ tiene
***[#orange] limite 
**** imferior 
*****_ es cuando 
****** Un límite superior es la cantidad mminimo  que una acción 
**** superior
*****_ es cuando 
****** Un límite superior es la cantidad máxima que una acción 
**_ es 
***[#orange] Un cambio 
***[#orange] rama de la matematica
***[#orange]  describir sucesos de la vida real en constante cambio 
**_ tiene 
***[#orange] represantacion cartensiana 
**** mediante los cauadrantes
**** con numeros reales positivos 
**** tiene imagen y donimio

@endmindmap
 ```
 [Funciones (2010)](https://canal.uned.es/video/5a6f2d09b1111f21778b457f)
 # La matemática del computador
 ## Descripcion 
 el mapa  trata sobre el funcionamiento matematico de un computador.

 ```plantuml
@startmindmap
<style>
mindmapDiagram{
   
    BackGroundColor  	#FAEBD7
    LineBorder none
    LineColor black
    :depth(1) {
        BackGroundColor  white
    }
    node{
        LineColor none
        
        BackGroundColor  #00FFFF
    }
}
</style>
title   matematica del computador.
*[#green] matematica del computador
**_ es 
***[#orange] la idea de error
****_ es
***** la aproximacion de un numero ral con una \n cierta cantidad de cifras finitas
**_ es 
***[#orange] truncar un numero 
****_ es
    * simplemente cortar unas cuantas cifras\n a la derecha para una nada
***[#orange] redondear un numero 
****_ es
    * un truncamiento refinado
***** los errores cometidos son los menores 
**_ se enlanza 
***[#orange]  con algebra booleane 
***[#orange]  sistema binario basico 
***[#orange]  representaciones graficas
****_ ejemplo
***** representaccion de graficos o circuitos electricos 
***[#orange]  Pertenencia 
***[#orange]  no pertenencia 

**_ se aplican 
***[#orange]   en la division binaria 
****_ que 
***** se tiene que sumar  \n un numero cada que se divide

**_ se centra en
***[#orange] sistemas 
****_ como 
***** octal 
******_ ocupa 
******* 8 numeros 
***** binario 
******_ ocupa 
******* 2 numeros 
***** hexadecimal 
******_ ocupa 
******* 6 numeros 
***** decimal 
******_ ocupa 
******* 10 numeros 
**_ representan
***[#orange] un numero 
****_ como 
***** representacion interna de numeros enteros
***** representacion en exeso 
***** representacion complemento a 2
***** representacion en magnitud  
**_ definicion de 
***[#orange] computador 
****_ se basa
***** en principios matematicas por teoremas de una calculadora 

**_ representan 
***[#orange] numeros 
****_ muy grandes
***** por medio de multiplicaciones 
******_ por ejemplo
******* notacion cientifica
****_ o muy chicos 
***** elevados a una potencia 

**_ utiliza el 
***[#orange] sistema binario 
**** es el mas basico 
****_ con operaciones 
***** suma
***** resta 
***** division
***** multiplcacion
****_ el cual aplica
***** el sistema base 2
**_ es 
***[#orange] codificacion 
**** se pueden representar 
***** numeros
***** letras 
****_ mediante 
***** 0 y 1 o encendido o apagado 
@endmindmap
 ```
 [La matemática del computador (2002)](https://canal.uned.es/video/5a6f1b76b1111f15098b45fa)
 [La matemática del computador (2002)](https://canal.uned.es/video/5a6f1b81b1111f15098b4640)
## Herramientas utilizadas
[Diagrams and flowcharts](https://docs.gitlab.com/ee/user/markdown.html#diagrams-and-flowcharts)
[PlantUML and GitLab](https://docs.gitlab.com/ee/administration/integration/plantuml.html)
[MindMap](https://plantuml.com/es/mindmap-diagram)
 ## Autor 
 > Agustin cruz Everardo alvaro